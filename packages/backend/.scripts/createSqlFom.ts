import { Mas_mysql, Mas_sqlite } from "mas-sql";
import path from "path";
import { sqlConfig } from "../config/sqlConfig/config";
import { SqlFormData } from "../config/sqlConfig/SqlFormData";
import { sqliteFormData } from "../config/sqlConfig/sqliteFormData";

async function createForm() {
  if (sqlConfig.useMysql) {
    const mas_sql = new Mas_mysql();
    await mas_sql.createSqlForm(sqlConfig.mysqlConfig, SqlFormData);
  }
  if (sqlConfig.useSqlite) {
    const keyArr = Object.keys(sqliteFormData);
    keyArr.forEach(async (key) => {
      const mas_sqlite = new Mas_sqlite();
      await mas_sqlite.createSqlForm(
        path.join(__dirname, `../data/sqlite/${key}.db`),
        sqliteFormData[key]
      );
    });
  }
}
createForm();
