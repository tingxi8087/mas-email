import type { MasReq, MasRes } from "mas-server";
import masState from "@@/test/src/masState";
import sqlform from "@@/test/src/sqlform.json";

export default async (req: MasReq, res: MasRes) => {
  const { sql } = masState;
  const sqlRes = await sql.use(sqlform.$form).delete(req.body).go();
  res.return(sqlRes);
};
