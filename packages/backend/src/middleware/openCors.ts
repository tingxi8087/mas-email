export const openCors = (eapp) => {
  const store = { allowHeader: [] };
  eapp.use((req, res, next) => {
    const origin = req.headers.origin;
    const requestedHeaders = req.headers["access-control-request-headers"];
    if (requestedHeaders) {
      store.allowHeader = [
        ...new Set([...store.allowHeader, ...requestedHeaders.split(",")]),
      ];
    }
    res.header("Access-Control-Allow-Origin", origin);
    res.header("Access-Control-Allow-Headers", store.allowHeader);
    res.header(
      "Access-Control-Allow-Methods",
      "PUT, POST, GET, DELETE, OPTIONS"
    );
    res.header("Access-Control-Allow-Credentials", "true");
    if (req.method === "OPTIONS") {
      res.sendStatus(204);
    } else {
      next();
    }
  });
};
