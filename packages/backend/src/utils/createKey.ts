import crypto from "crypto";
import path from "path";
import fs from "fs";
export function generateEmailKey() {
  // 生成密钥对
  const { privateKey, publicKey } = crypto.generateKeyPairSync("rsa", {
    modulusLength: 2048,
    publicKeyEncoding: {
      type: "spki",
      format: "pem",
    },
    privateKeyEncoding: {
      type: "pkcs8",
      format: "pem",
    },
  });
  const outPath = path.join(__dirname, "../../data");
  fs.writeFileSync(path.join(outPath, "private.key"), privateKey);
  fs.writeFileSync(path.join(outPath, "public.key"), publicKey);
  return {
    privateKey,
    publicKey,
  };
}

export function getKeys() {
  const outPath = path.join(__dirname, "../../data");
  const privateKeyPath = path.join(outPath, "private.key");
  const publicKeyPath = path.join(outPath, "public.key");

  // 检查文件是否都存在
  if (!fs.existsSync(privateKeyPath) || !fs.existsSync(publicKeyPath)) {
    return null;
  }

  try {
    const privateKey = fs.readFileSync(privateKeyPath, "utf8");
    const publicKey = fs.readFileSync(publicKeyPath, "utf8");
    return {
      privateKey,
      publicKey,
    };
  } catch (error) {
    return null;
  }
}
