import { sqlConfig } from "@@/config/sqlConfig/config";

import { Mas_mysql, Mas_sqlite, Massql } from "mas-sql";
import path from "path";
import fs from "fs";
import { state } from "@/state";
// 打印数据库结果
Massql.debugMode = 2;
export async function init_masSql() {
  if (sqlConfig.useMysql) {
    const mas_mysql = new Mas_mysql();
    // 初始化数据库
    await mas_mysql.sqlInit(sqlConfig.mysqlConfig);
    // 检查数据库表是否符合规范，不符合则更改数据表
    await mas_mysql.checkSqlKey();
    // 根据数据库表生成一个json文件，用于代码提示，可注释掉
    await mas_mysql.getsqlFormData(path.join(__dirname, "../../sqlForm.json"));
    // 获得数据库对象
    const sql = () => mas_mysql.getMassql();
    state.Sql = sql;
  }
  if (sqlConfig.useSqlite) {
    const sqlitePath = path.join(__dirname, "../../../data/sqlite/");
    if (!fs.existsSync(sqlitePath)) {
      fs.mkdirSync(sqlitePath, { recursive: true });
    }
    const sqliteList = fs
      .readdirSync(sqlitePath)
      .filter((item) => /^\S+\.db$/.test(item));
    sqliteList.forEach(async (sqliteKey) => {
      const mas_sqlite = new Mas_sqlite();
      // 初始化数据库
      await mas_sqlite.sqlInit(path.join(sqlitePath, sqliteKey));
      // 检查数据库表是否符合规范，不符合则更改数据表
      await mas_sqlite.checkSqlKey();
      // 根据数据库表生成一个json文件，用于代码提示，可跳过
      await mas_sqlite.getsqlFormData(
        path.join(__dirname, `../../sqliteFormObj/${sqliteKey}.json`)
      );
      // 获得数据库对象
      const sql = () => mas_sqlite.getMassql();
      state.sqlite[sqliteKey] = sql;
      if (sqliteKey == sqlConfig.mainSqlite + ".db") {
        state.Sqle = sql;
        // 根据数据库表生成一个json文件，用于代码提示，可跳过
        await mas_sqlite.getsqlFormData(
          path.join(__dirname, `../../sqliteForm.json`)
        );
      }
    });
  }
}
