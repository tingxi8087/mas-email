import { state } from "@/state";
import Dirty from "dirty";
import path from "path";
import fs from "fs";

export const initDirty = () => {
  const dbPath = path.join(__dirname, "../../data/Dirty.db");
  const dirPath = path.dirname(dbPath);
  // 确保目录存在,不存在则创建
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true });
  }
  // 确保文件存在
  if (!fs.existsSync(dbPath)) {
    fs.writeFileSync(dbPath, "");
  }
  const db = new Dirty(dbPath);
  state.Dirty = db;
};
