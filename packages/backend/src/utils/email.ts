const { spawn } = require("child_process");
let serverProcess = null;

// 启动邮件服务器
export function startEmailServer(workDir: string, pyArgs: string[]) {
  return new Promise((resolve, reject) => {
    try {
      serverProcess = spawn("python", pyArgs, {
        cwd: workDir,
        // 添加这些选项以确保正确处理输出
        stdio: ["pipe", "pipe", "pipe"],
        shell: true,
      });

      // 设置编码以正确处理输出
      serverProcess.stdout.setEncoding("utf8");
      serverProcess.stderr.setEncoding("utf8");

      // 输出标准输出
      serverProcess.stdout.on("data", (data: string) => {
        if (data.includes("start email server")) {
          resolve(true);
        }
        const output = data.toString().trim();
        if (output) {
          console.log("email server:", output);
        }
      });

      // 输出错误信息
      serverProcess.stderr.on("data", (data: string) => {
        const error = data.toString().trim();
        if (error) {
          console.error("服务器错误:", error);
        }
      });

      // 监听进程错误
      serverProcess.on("error", (error) => {
        console.error("进程错误:", error);
        resolve(false);
      });

      // 监听进程退出
      serverProcess.on("close", (code) => {
        if (code !== 0) {
          console.log(`服务器进程异常退出，退出码: ${code}`);
        } else {
          console.log(`服务器进程正常退出，退出码: ${code}`);
        }
      });

      // 检查服务器是否成功启动
      // setTimeout(() => {
      //   if (!serverProcess || serverProcess.killed) {
      //     reject(new Error("服务器启动失败"));
      //   } else {
      //     console.log("邮件服务器已成功启动");
      //     resolve(serverProcess);
      //   }
      // }, 2000);
    } catch (error) {
      console.error("启动服务器时发生错误:", error);
      reject(error);
    }
  });
}

// 停止邮件服务器
export function stopEmailServer() {
  return new Promise((resolve, reject) => {
    if (!serverProcess) {
      resolve(false);
      return;
    }
    try {
      // 使用 SIGTERM 信号更温和地结束进程
      serverProcess.kill("SIGTERM");

      // 设置超时确保进程被终止
      const timeout = setTimeout(() => {
        if (serverProcess) {
          serverProcess.kill("SIGKILL");
        }
      }, 5000);

      serverProcess.on("exit", () => {
        clearTimeout(timeout);
        // console.log("邮件服务器已正常停止");
        serverProcess = null;
        resolve(true);
      });
    } catch (error) {
      console.error("停止邮件服务器时出错:", error);
      resolve(false);
    }
  });
}
// 获取邮件服务器状态
export function getEmailServerStatus() {
  return new Promise<{ running: boolean; pid: number | null }>((resolve) => {
    if (!serverProcess || serverProcess.killed) {
      resolve(null);
      return;
    }

    resolve(serverProcess.pid || null);
  });
}
// 导出函数
module.exports = {
  startEmailServer,
  stopEmailServer,
  getEmailServerStatus,
};
