import { state } from "@/state";
import cache from "memory-cache";
import path from "path";
export const initMemoryCache = () => {
  const db = new cache.Cache();
  state.cache = db;
};
