import { state } from "./state";
import sqlForm from "@/sqlForm.json";
import { randomUUID } from "crypto";
const { Sql, Sqle, Dirty } = state;
import fs from "fs";
import path from "path";
import { generateEmailKey } from "./utils/createKey";
// import { startEmailServer } from "./utils/email";

// generateEmailKey();
// 使用sqllite
// Sqle().use(sqlForm.student).insert({ name: "sdsd", age: 10 }).go();
// Sqle().use(sqlForm.student).select().go();

// 使用mysql
// Sql().use(sqlForm.teacher).delete(9).go();
// Sql().use(sqlForm.teacher).select().go();

// 使用Dirty
// state.Dirty.set("john", { eyes: "blue" });
// console.log(Dirty.get("john"));

// 使用缓存
// state.cache.put("foo", "bar");
// console.log(state.cache.get("foo"), "无限缓存");
// state.cache.put("foot", "foot", 1000);
// console.log(state.cache.get("foot"), "有这个缓存");
// setTimeout(() => {
//   console.log(state.cache.get("foot"), "缓存失效了");
// }, 2000);
