import {
  createToken,
  type ApiConfigTypeItem,
  type MasReq,
  type MasRes,
} from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "权限测试接口",
  methods: "post",
  permission: ["admin"],
};
const token = createToken(
  { data: "这是一条token里的信息", permission: ["admin"] },
  "8087"
);
// 开启这行拿到token，然后请求头里加上token即可正常访问
// console.log(token);
export default async (req: MasReq, res: MasRes) => {
  console.log(res.token);
  res.return("你是超级管理员!");
};

// 也可以选择自己效验
// import { createToken, validToken } from "mas-server";
// const token = createToken({
//   time: 10,
//   permission: ["admin", "www"],
//   data: { a: 1, b: 2 },
// });
// setTimeout(() => {
//   const tokenInfo = validToken({ token, permission: ["user", "admin"] });
//   console.log(token);
//   console.log(tokenInfo);
// }, 1000);
