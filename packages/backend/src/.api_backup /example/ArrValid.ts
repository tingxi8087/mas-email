import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "效验数组",
  methods: "post",
  validFormat: [Number],
  example: [1, 2],
};
export default async (req: MasReq, res: MasRes) => {
  console.log(req.body);
  res.return(req.body);
};
