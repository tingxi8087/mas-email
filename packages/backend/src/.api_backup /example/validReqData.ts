import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "数据效验及响应示例接口",
  methods: "post",
  validFormat: {
    name: String,
    age: Number,
    grade: [{ name: String, score: Number }],
    desc: String,
    family: {
      mother: {
        name: String,
        age: Number,
      },
      fahter: {
        name: String,
        age: Number,
        t: Boolean,
      },
    },
  },
  // 效验严格模式，默认不开启，开启则不能携带额外参数
  validFormatStrict: false,
  // 接口文档的请求示例
  example: {
    extra: "这是一个额外参数，如果开启严格模式且带上这个参数将会不通过效验",
    name: "小蔡",
    age: 18,
    grade: [{ name: "篮球", score: 99 }],
    desc: "两年半",
    family: {
      mother: {
        name: "母坤",
        age: 40,
      },
      fahter: {
        name: "公坤",
        age: 40,
        t: true,
      },
    },
  },
  // 响应示例
  resExample: {
    msg: "",
    status: 1,
    data: "hello!练习两年半的程序员",
  },
  validFormatDes: [
    {
      name: "extra",
      des: "额外的参数",
      type: String,
    },
    {
      name: "name",
      des: "姓名",
    },
    {
      name: "age",
      des: "年龄",
    },
    {
      name: "grade",
      des: "成绩",
    },
    {
      name: "family",
      des: "家庭情况",
    },
  ],
  des: "详细数据效验及响应示例接口",
  header: [
    {
      name: "boom",
      type: String,
      des: "一个可不传的请求头参数",
      optional: true,
    },
  ],
};
export default async (req: MasReq, res: MasRes) => {
  res.return("hello!练习两年半的程序员");
};
