import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "测试接口",
  methods: "get",
};
export default async (req: MasReq, res: MasRes) => {
  res.return("测试接口!");
};
