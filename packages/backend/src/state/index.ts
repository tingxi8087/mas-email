import { Massql } from "mas-sql";

export const state: {
  Sql: () => Massql;
  Sqle: () => Massql;
  Dirty: any;
  cache: any;
  sqlite: {
    [key in string]: () => Massql;
  };
  ENV: "pro" | "dev" | "";
} = {
  Sql: null,
  Sqle: null,
  sqlite: {},
  Dirty: null,
  cache: null,
  ENV: "",
};
