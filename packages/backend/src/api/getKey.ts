import { getKeys } from "@/utils/createKey";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "获取密钥",
  methods: "get",
  permission: ["admin"],
};
export default async (req: MasReq, res: MasRes) => {
  res.reply(getKeys());
};
