import { state } from "@/state";
import {
  createToken,
  type ApiConfigTypeItem,
  type MasReq,
  type MasRes,
} from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "登录接口",
  methods: "post",
  validFormat: {
    username: String,
    password: String,
  },
  example: {
    username: "123",
    password: "123",
  },
};
export default async (req: MasReq, res: MasRes) => {
  const admin = state.Dirty.get("admin");
  const tokenKey = state.Dirty.get("tokenKey");
  // console.log(tokenKey,"##");
  if (
    admin &&
    admin.username === req.body.username &&
    admin.password === req.body.password &&
    tokenKey
  ) {
    res.reply({
      token: createToken(
        { data: { admin }, permission: ["admin"], time: 60 * 60 * 24 },
        tokenKey
      ),
    });
  } else if (!admin) {
    res.fail(null, tokenKey ? "请先注册账户" : "请先开启邮件服务器");
  } else {
    res.fail(null, "用户名或密码错误");
  }
};
