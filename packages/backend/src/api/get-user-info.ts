import { state } from "@/state";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "获取用户信息",
  methods: "get",
  permission: ["admin"],
};
export default async (req: MasReq, res: MasRes) => {
  const admin = state.Dirty.get("admin");
  res.return({ username: admin?.username }, !!admin);
};
