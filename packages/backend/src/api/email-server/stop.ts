import { stopEmailServer } from "@/utils/email";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "停止服务器",
  methods: "get",
  permission: ["admin"],
};
export default async (req: MasReq, res: MasRes) => {
  const result = await stopEmailServer();
  res.reply(result);
};
