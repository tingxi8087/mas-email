import { getEmailServerStatus, startEmailServer } from "@/utils/email";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
import path from "path";
export const config: ApiConfigTypeItem = {
  name: "开启服务器",
  methods: "get",
  permission: ["admin"],
};
export default async (req: MasReq, res: MasRes) => {
  if (await getEmailServerStatus()) {
    res.return(false, 0, 200, "服务器已开启");
    return;
  }
  const testArgs = [
    "./server.py",
    "--key_path",
    "../data/private.key",
    "--db_path",
    "../data/emails.db",
    "--host",
    "tingxi8087.com",
  ];
  if (req.query.debug) {
    testArgs.push("--debug");
  }
  const result = await startEmailServer(
    path.join(__dirname, "../../../email-server"),
    testArgs
  );
  res.reply(result);
};
