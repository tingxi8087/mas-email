import { getEmailServerStatus } from "@/utils/email";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "查看服务器状态",
  methods: "get",
  permission: ["admin"],
};
export default async (req: MasReq, res: MasRes) => {
  const result = await getEmailServerStatus();
  res.reply(result);
};
