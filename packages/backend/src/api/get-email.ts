import { getEmailServerStatus } from "@/utils/email";
import axios from "axios";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "获取邮件",
  methods: "get",
  permission: ["admin"],
  validFormat: {
    // sender: String,
    // recipient: String,
    // subject: String,
    // content: String,
    // start_date: String,
    // end_date: String,
    // type: String,
    // current: Number,
    // size: Number,
  },
};
export default async (req: MasReq, res: MasRes) => {
  if (await getEmailServerStatus()) {
    axios
      .get("http://127.0.0.1:11101/receive", { params: req.query })
      .then((result) => {
        res.return(
          result.data?.data || {},
          result.data?.status == "success",
          200
        );
      })
      .catch((error) => {
        res.fail(error);
      });
  } else {
    res.fail(null, "请先开启邮件服务器");
  }
};
