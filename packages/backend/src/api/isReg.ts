import { state } from "@/state";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "是否已经注册",
  methods: "get",
};
export default async (req: MasReq, res: MasRes) => {
  const admin = state.Dirty.get("admin");
  res.reply(admin ? true : false);
};
