import { getEmailServerStatus } from "@/utils/email";
import axios from "axios";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "发送邮件",
  methods: "post",
  permission: ["admin"],
  validFormat: {
    from: String,
    to: String,
    subject: String,
    content: String,
  },
};
export default async (req: MasReq, res: MasRes) => {
  if (await getEmailServerStatus()) {
    axios
      .post("http://127.0.0.1:11101/send", req.body)
      .then((result) => {
        res.return(
          result.data?.status == "success",
          result.data?.status == "success",
          200,
          result.data?.message
        );
      })
      .catch((error) => {
        res.fail(error);
      });
  } else {
    res.fail(false, "请先开启邮件服务器");
  }
};
