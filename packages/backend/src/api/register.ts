import { state } from "@/state";
import serverConfig from "@@/config/serverConfig/config";
import { randomUUID } from "crypto";
import type { ApiConfigTypeItem, MasReq, MasRes } from "mas-server";
export const config: ApiConfigTypeItem = {
  name: "注册接口",
  methods: "post",
  validFormat: {
    username: String,
    password: String,
  },
  example: {
    username: "123",
    password: "123",
  },
};
export default async (req: MasReq, res: MasRes) => {
  const admin = state.Dirty.get("admin");
  if (admin) {
    return res.fail(false, "已存在账户");
  } else {
    state.Dirty.set("tokenKey", randomUUID().replace(/-/g, ""));
    serverConfig.token.pwd = state.Dirty.get("tokenKey");
    state.Dirty.set("admin", req.body, () => {
      res.success(true);
    });
  }
};
