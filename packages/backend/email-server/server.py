import argparse
from aiosmtpd.controller import Controller
from aiosmtpd.handlers import Message
from flask import Flask, request, jsonify
import sqlite3
import email
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import dkim
import dns.resolver
import traceback
# 使用 Gunicorn 运行
from gunicorn.app.base import BaseApplication  # type: ignore
app = Flask(__name__)

import sys

# 保存原始的print函数
original_print = print

# 重写print函数
def print(*args, **kwargs):
    # 启动消息始终输出，不受 DEBUG 影响
    if "start email server" in str(args):
        sys.stdout.write("start email server\n")
        sys.stdout.flush()
        return
    # 其他消息根据 DEBUG 标志决定是否输出
    if DEBUG:
        # 将所有参数转换为字符串并用空格连接
        message = " ".join(str(arg) for arg in args)
        sys.stdout.write(message + "\n")
        sys.stdout.flush()
        # 如果有原始print的特殊参数，也执行原始print
        if kwargs:
            original_print(*args, **kwargs)

# 确保在定义全局变量时设置默认值
DEBUG = False  # 默认关闭调试输出

# 数据库初始化

def init_db():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute(
        """
        CREATE TABLE IF NOT EXISTS emails
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
         sender TEXT,
         recipient TEXT,
         subject TEXT,
         content TEXT,
         type TEXT,
         timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)
    """
    )
    conn.commit()
    conn.close()


# SMTP处理器
class EmailHandler:
    def __init__(self):
        # 读取DKIM私钥
        with open(PRIVATE_KEY_PATH, "rb") as f:
            self.dkim_private_key = f.read()

    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        # 收件人允许任何域名的邮箱地址
        if not "@" in address:  # 基本的邮箱格式验证
            return "550 Invalid email format"
        envelope.rcpt_tos.append(address)
        return "250 OK"

    async def handle_MAIL(self, server, session, envelope, address, mail_options):
        # 发件人必须是配置的域名
        # if not address.endswith(f"@{EMAIL_DOMAIN}"):
        #     return '550 Not a valid sender'
        envelope.mail_from = address
        return "250 Sender OK"

    async def handle_DATA(self, server, session, envelope):
        try:
            print(f"Received mail from: {envelope.mail_from}")
            print(f"Recipients: {envelope.rcpt_tos}")

            message = email.message_from_bytes(envelope.content)

            # 解码邮件主题
            subject = message.get("subject", "")
            try:
                # 解码邮件主题
                decoded_subject = email.header.decode_header(subject)
                subject = ""
                for text, charset in decoded_subject:
                    if isinstance(text, bytes):
                        if charset:
                            subject += text.decode(charset)
                        else:
                            # 尝试常用编码
                            for encoding in ["utf-8", "gb18030", "gbk", "gb2312"]:
                                try:
                                    subject += text.decode(encoding)
                                    break
                                except UnicodeDecodeError:
                                    continue
                    else:
                        subject += text
            except Exception as e:
                print(f"Error decoding subject: {str(e)}")
                subject = "无法解码的主题"

            # 获取邮件内容，增加错误处理和多重编码尝试
            try:
                if message.is_multipart():
                    content = ""
                    for part in message.walk():
                        if part.get_content_type() == "text/plain":
                            # 获取编码
                            charset = part.get_content_charset() or "utf-8"
                            try:
                                # 尝试使用指定的编码
                                payload = part.get_payload(decode=True)
                                content += payload.decode(charset,
                                                          errors="replace")
                            except UnicodeDecodeError:
                                # 如果失败，尝试其他常见编码
                                for encoding in [
                                    "utf-8",
                                    "gbk",
                                    "gb2312",
                                    "iso-8859-1",
                                ]:
                                    try:
                                        content += payload.decode(
                                            encoding, errors="replace"
                                        )
                                        break
                                    except UnicodeDecodeError:
                                        continue
                else:
                    # 单一内容的邮件
                    charset = message.get_content_charset() or "utf-8"
                    try:
                        payload = message.get_payload(decode=True)
                        content = payload.decode(charset, errors="replace")
                    except UnicodeDecodeError:
                        # 尝试其他编码
                        for encoding in ["utf-8", "gbk", "gb2312", "iso-8859-1"]:
                            try:
                                content = payload.decode(
                                    encoding, errors="replace")
                                break
                            except UnicodeDecodeError:
                                continue
                        else:
                            # 如果所有编码都失败，使用 replace 模式
                            content = payload.decode("utf-8", errors="replace")
            except Exception as e:
                print(f"Error decoding content: {str(e)}")
                content = "无法解码的邮件内容"

            try:
                # DKIM 签名
                sig = dkim.sign(
                    message=envelope.content,
                    selector=b"default",
                    domain=EMAIL_DOMAIN.encode(),
                    privkey=self.dkim_private_key,
                    include_headers=[b"From", b"To", b"Subject"],
                )

                # 添加 DKIM-Signature 头
                if isinstance(sig, bytes):
                    dkim_header = sig[len(
                        b"DKIM-Signature: "):].decode("utf-8")
                else:
                    dkim_header = sig[len("DKIM-Signature: "):]
                message["DKIM-Signature"] = dkim_header

                print("DKIM signature added successfully")

            except Exception as e:
                print(f"DKIM signing error: {str(e)}")
                traceback.print_exc()

            # 实际发送邮件到目标服务器
            delivery_success = False
            try:
                for rcpt in envelope.rcpt_tos:
                    domain = rcpt.split("@")[1]
                    if domain == EMAIL_DOMAIN:
                        # 如果是发送到本地域名，存储到数据库
                        print(f"Local delivery for {rcpt}")
                        try:
                            conn = sqlite3.connect(DB_PATH)
                            c = conn.cursor()
                            c.execute(
                                """
                                INSERT INTO emails (sender, recipient, subject, content, type)
                                VALUES (?, ?, ?, ?, ?)
                                """,
                                (
                                    envelope.mail_from,
                                    rcpt,  # 使用具体的收件人
                                    subject,
                                    content,
                                    "receive"  # 标记为接收
                                ),
                            )
                            conn.commit()
                            conn.close()
                            print(f"Saved to database for {rcpt}")
                            delivery_success = True
                        except Exception as e:
                            print(f"Error saving to database: {str(e)}")
                            traceback.print_exc()
                        continue

                    print(f"Looking up MX records for {domain}")
                    try:
                        mx_records = dns.resolver.resolve(domain, "MX")
                        mx_hosts = sorted(
                            [(r.preference, str(r.exchange))
                             for r in mx_records]
                        )
                    except dns.resolver.NoAnswer:
                        print(
                            f"No MX record found for {domain}, using A record")
                        a_records = dns.resolver.resolve(domain, "A")
                        mx_hosts = [(10, str(r)) for r in a_records]

                    # 尝试每个服务器
                    for preference, mx_host in mx_hosts:
                        try:
                            print(f"Attempting to deliver to {mx_host}")
                            with smtplib.SMTP(mx_host, timeout=30) as smtp:
                                smtp.starttls()
                                smtp.ehlo()
                                smtp.send_message(message)
                                print(
                                    f"Successfully delivered to {rcpt} via {mx_host}")
                                delivery_success = True
                                break
                        except Exception as e:
                            print(f"Failed to deliver via {mx_host}: {str(e)}")
                            continue

            except Exception as e:
                print(f"Error in mail delivery: {str(e)}")
                if not delivery_success:
                    traceback.print_exc()

            # 如果是本地投递或者外部投递成功，返回成功
            if delivery_success:
                return "250 Message accepted for delivery"
            else:
                return "500 Error processing message"

        except Exception as e:
            print(f"Handle DATA error: {str(e)}")
            return "500 Error processing message"


# Flask路由
@app.route("/send", methods=["POST"])
def send_email():
    data = request.json
    print(f"Attempting to send email: {data}")

    required_fields = ["from", "to", "subject", "content"]

    # 检查必填字段
    for field in required_fields:
        if field not in data:
            return (
                jsonify({"status": "error", "message": f"缺少必填字段: {field}"}),
                400,
            )

    # 只验证发件人域名
    if not data["from"].endswith(f"@{EMAIL_DOMAIN}"):
        return jsonify({"status": "error", "message": "发件人邮箱域名不正确"}), 400

    # 验证收件人邮箱格式
    if not "@" in data["to"]:
        return jsonify({"status": "error", "message": "收件人邮箱格式不正确"}), 400

    try:
        # 创建邮件
        msg = MIMEMultipart()
        msg["From"] = data["from"]
        msg["To"] = data["to"]
        msg["Subject"] = data["subject"]
        msg["Date"] = email.utils.formatdate()

        # 添加重要的邮件头
        msg["Message-ID"] = email.utils.make_msgid(domain=EMAIL_DOMAIN)
        msg["Return-Path"] = data["from"]
        msg["X-Mailer"] = "Python Email Server"

        # 添加邮件内容
        msg.attach(MIMEText(data["content"], "plain", "utf-8"))

        # 存储到数据库
        try:
            conn = sqlite3.connect(DB_PATH)
            c = conn.cursor()
            c.execute(
                """
                INSERT INTO emails (sender, recipient, subject, content, type)
                VALUES (?, ?, ?, ?, ?)
                """,
                (
                    data["from"],
                    data["to"],
                    data["subject"],
                    data["content"],
                    "send"  # 标记为发送
                ),
            )
            conn.commit()
            conn.close()
            print(f"Saved outgoing email to database")
        except Exception as e:
            print(f"Error saving outgoing email to database: {str(e)}")
            traceback.print_exc()

        # 发送邮件
        with smtplib.SMTP("127.0.0.1", 25, timeout=30) as smtp:
            print(f"Connecting to SMTP server...")
            smtp.send_message(msg)
            print(f"Email sent successfully")

        return jsonify({"status": "success", "message": "邮件已发送"}), 200

    except Exception as e:
        print(f"Error sending email: {str(e)}")
        return jsonify({"status": "error", "message": str(e)}), 500


@app.route("/receive", methods=["GET"])
def receive_email():
    try:
        # 获取分页参数
        page = request.args.get('current', 1, type=int)
        per_page = request.args.get('size', 10, type=int)

        # 获取搜索条件
        sender = request.args.get('sender')
        recipient = request.args.get('recipient')
        subject = request.args.get('subject')
        content = request.args.get('content')
        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')
        type = request.args.get('type')
        id = request.args.get('id')
        # 构建查询条件
        conditions = []
        params = []
        if id:
            conditions.append("id = ?")
            params.append(id)
        if sender:
            conditions.append("sender LIKE ?")
            params.append(f"%{sender}%")
        if recipient:
            conditions.append("recipient LIKE ?")
            params.append(f"%{recipient}%")
        if subject:
            conditions.append("subject LIKE ?")
            params.append(f"%{subject}%")
        if content:
            conditions.append("content LIKE ?")
            params.append(f"%{content}%")
        if start_date:
            conditions.append("datetime(timestamp, 'localtime') >= datetime(?)")
            params.append(f"{start_date}")
        if end_date:
            conditions.append("datetime(timestamp, 'localtime') <= datetime(?)")
            params.append(f"{end_date}")
        if type:
            conditions.append("type = ?")
            params.append(type)

        # 构建 WHERE 子句
        where_clause = " AND ".join(conditions) if conditions else "1=1"

        # 计算偏移量
        offset = (page - 1) * per_page

        conn = sqlite3.connect(DB_PATH)
        c = conn.cursor()

        # 获取总记录数
        count_query = f"SELECT COUNT(*) FROM emails WHERE {where_clause}"
        c.execute(count_query, params)
        total = c.fetchone()[0]

        # 分页查询
        query = f"""
            SELECT id, sender, recipient, subject, content, 
                   datetime(timestamp, 'localtime') as timestamp,
                   type
            FROM emails 
            WHERE {where_clause}
            ORDER BY timestamp DESC
            LIMIT ? OFFSET ?
        """

        # 添加分页参数
        c.execute(query, params + [per_page, offset])
        emails = c.fetchall()
        conn.close()

        return jsonify({
            "status": "success",
            "data": {
                "total": total,
                "current": page,
                "size": per_page,
                "emails": [
                    {
                        "id": email[0],
                        "sender": email[1],
                        "recipient": email[2],
                        "subject": email[3],
                        "content": email[4],
                        "timestamp": email[5],
                        "type": email[6],
                    }
                    for email in emails
                ],
            }
        }), 200
    except Exception as e:
        print(f"查询错误: {str(e)}")
        return jsonify({"status": "error", "message": str(e)}), 500


def get_args():
    parser = argparse.ArgumentParser(description='命令行参数示例')
    parser.add_argument('--host', type=str, help='邮箱地址', required=True)
    parser.add_argument('--key_path', type=str, help='DKIM私钥路径', required=True)
    parser.add_argument('--db_path', type=str, help='数据库路径', required=True)
    parser.add_argument('--debug', action='store_true', help='是否开启调试打印')
    args = parser.parse_args()
    return {
        "host": args.host, 
        "key_path": args.key_path, 
        "db_path": args.db_path,
        "debug": args.debug
    }

def main(args):
    global EMAIL_DOMAIN
    global PRIVATE_KEY_PATH
    global DB_PATH
    global DEBUG
    EMAIL_DOMAIN = args["host"]
    PRIVATE_KEY_PATH = args["key_path"]
    DB_PATH = args["db_path"]
    DEBUG = args["debug"]
    print("args", str(args))
    init_db()
    # SMTP服务器
    controller = Controller(
        EmailHandler(),
        hostname="0.0.0.0",
        port=25,
        server_hostname=f"mail.{EMAIL_DOMAIN}",
    )
    controller.start()

    class StandaloneApplication(BaseApplication):
        def __init__(self, app, options=None):
            self.options = options or {}
            self.application = app
            super().__init__()

        def load_config(self):
            for key, value in self.options.items():
                self.cfg.set(key, value)

        def load(self):
            return self.application

    options = {
        'bind': '0.0.0.0:11101',
        'workers': 1,
        'worker_class': 'sync',
        'loglevel': 'warning'
    }

    StandaloneApplication(app, options).run()


if __name__ == "__main__":
    print("start email server")
    args = get_args()
    main(args)
