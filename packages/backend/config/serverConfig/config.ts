import { ConfigType } from "mas-server";
import path from "path";
export const serverConfig: ConfigType = {
  projectName: "mas-server",
  port: 8087,
  logs: {
    open: true,
    debug: false,
  },
  apiLimit: {
    open: true,
    windowMs: 60 * 1000,
    max: 1000,
  },
  token: {
    open: true,
    pwd: "8087",
    headerParams: "token",
  },
  debugTime: 100,
};
// 配置路径
export const configPath = {
  configPath: path.join(__dirname, "./config.ts"),
  apisPath: path.join(__dirname, "../../src/api/"),
  defalutApiPath: path.join(__dirname, "../../src/.apiTemplate/defaultApi.ts"),
  logPath: path.join(__dirname, "../../src/logs"),
  degbugPath: path.join(__dirname, "../../src/debug.ts"),
};
export default serverConfig;
