import { CreateSqlFormType } from "mas-sql";
// mysql数据库表
export const SqlFormData: CreateSqlFormType = {
  testForm: [
    {
      key: "testA",
      type: "str",
    },
    {
      key: "numB",
      type: "float",
    },
  ],
};
