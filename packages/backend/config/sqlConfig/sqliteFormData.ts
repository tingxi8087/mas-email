import { CreateSqlFormType } from "mas-sql";
// sqlite数据库表
export const sqliteFormData: {
  [key in string]: CreateSqlFormType;
} = {
  test: {
    student: [
      {
        key: "name",
        type: "str",
      },
    ],
  },
  mas_auth: {
    test: [
      {
        key: "name",
        type: "str",
      },
    ],
  },
};
