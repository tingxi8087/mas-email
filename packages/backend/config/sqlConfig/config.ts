import path from "path";

export const sqlConfig = {
  // 调试模式，0为关闭，1的时候打印sql语句，2的时候打印结果
  debugMode: 2,
  useMysql: false,
  mysqlConfig: {
    host: "127.0.0.1",
    user: "root",
    password: "123456",
    database: "school",
  },
  useSqlite: true,
  mainSqlite: "test",
};
