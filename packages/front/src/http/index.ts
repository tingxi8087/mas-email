import { mReq } from "./mRequest";
import { req } from "./request";

export const getUserInfoHttp = () => req.get("/api/get-user-info");
export const isRegHttp = () => req.get("/api/isReg");
export const regHttp = (data: any) => req.post("/api/register", data);
export const loginHttp = (data: any) => req.post("/api/login", data);
export const generateTokenHttp = () => mReq.get("/api/createKey");
export const getKeyHttp = () => req.get("/api/getKey");
export const getEmailServerStatusHttp = () =>
  req.get("/api/email-server/status");
export const startEmailServerHttp = () => mReq.get("/api/email-server/start");
export const stopEmailServerHttp = () => req.get("/api/email-server/stop");
export const getReceiveListHttp = (data: any) =>
  req.get("/api/get-email", { params: { ...data, type: "receive" } });
export const getSendListHttp = (data: any) =>
  req.get("/api/get-email", { params: { ...data, type: "send" } });
export const sendEmailHttp = (data: any) =>
  req.post("/api/send-email", data);
