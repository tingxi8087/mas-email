import Axios from "axios";
const axios = Axios.create();
// axios.defaults.baseURL = "http://localhost:6900";
axios.interceptors.request.use((config) => {
  if (localStorage.getItem("token")) {
    config.headers.token = localStorage.getItem("token");
  }
  return config;
});
axios.interceptors.response.use(
  (res) => {
    return res?.data;
  },
  (err) => {
    return err?.response?.data;
    // if (!err?.response) {
    //   notification.error({
    //     message: "请求失败",
    //     description: `${err?.config?.url}`,
    //   });
    // } else {
    //   try {
    //     notification.error({
    //       message: "请求失败",
    //       description: `${err?.config?.url} ${
    //         err?.response?.status
    //       } ${JSON.stringify(err?.response?.data)}`,
    //     });
    //   } catch (error) {
    //     notification.error({
    //       message: "请求失败",
    //       description: `${err?.config?.url} ${err?.response?.status}`,
    //     });
    //   }
    // }
  }
);
export const req = axios;
