import { Button, Card } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";

export default function Settings() {
  const navigate = useNavigate();
  return (
    <Card>
      <Button onClick={() => navigate("/setConfig")}>进入引导页</Button>
    </Card>
  );
}
