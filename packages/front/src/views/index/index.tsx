import {
  getEmailServerStatusHttp,
  startEmailServerHttp,
  stopEmailServerHttp,
} from "@/http";
import { ReloadOutlined } from "@ant-design/icons";
import { Button, Card, message } from "antd";
import { useEffect, useState } from "react";

export default function Index() {
  const [emailServerStatus, setEmailServerStatus] = useState("停止");
  const [emailServerLoading, setEmailServerLoading] = useState(false);
  const getEmailServerStatus = () => {
    getEmailServerStatusHttp().then((res: any) => {
      console.log(res.data ? "开启" : "停止", "##");
      setEmailServerStatus(res.data ? "开启" : "停止");
    });
  };
  useEffect(() => {
    getEmailServerStatus();
  }, []);
  return (
    <Card className="flex h-4x">
      <div className="font-16px flex-y-c flex">
        <div>邮箱服务状态：</div>
        <div
          style={{ color: emailServerStatus == "开启" ? "#165dff" : "red" }}
          className="translatey--1"
        >
          {emailServerStatus}
        </div>
        <div
          className="pointer ml-1m"
          onClick={() => {
            getEmailServerStatus();
            message.destroy();
            message.success("刷新成功");
          }}
        >
          <ReloadOutlined />
        </div>
      </div>
      <div className="mt-1">
        <Button
          type="primary"
          disabled={emailServerStatus == "开启"}
          onClick={() => {
            setEmailServerLoading(true);
            startEmailServerHttp().then((res: any) => {
              setEmailServerLoading(false);
              getEmailServerStatus();
            });
          }}
          loading={emailServerLoading}
        >
          开启邮箱服务
        </Button>
        <Button
          className="ml-1"
          disabled={emailServerStatus !== "开启"}
          loading={emailServerLoading}
          onClick={() => {
            setEmailServerLoading(true);
            stopEmailServerHttp().then((res: any) => {
              setEmailServerLoading(false);
              getEmailServerStatus();
            });
          }}
        >
          关闭邮箱服务
        </Button>
      </div>
      {/* <h1>欢迎使用mas-email</h1> */}
    </Card>
  );
}
