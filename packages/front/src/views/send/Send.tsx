import { getReceiveListHttp, getSendListHttp } from "@/http";
import { Button, Card, message, Table } from "antd";
import React, { useEffect, useState } from "react";
import { ProTable } from "@ant-design/pro-components";
import DeatilModal, { deatilData } from "./DeatilModal";
export default function Send() {
  return (
    <Card>
      <DeatilModal />
      <ProTable
        search={{
          labelWidth: "auto",
          // 默认展开
          defaultCollapsed: false,
        }}
        columns={[
          {
            title: "ID",
            dataIndex: "id",
            width: 100,
            colSize: 0.4,
          },
          {
            title: "主题",
            dataIndex: "subject",
            ellipsis: true,
            order: 1,
          },
          {
            title: "内容",
            search: false,
            dataIndex: "content",
            ellipsis: true,
          },
          {
            title: "时间",
            dataIndex: "timestamp",
            width: 160,
            hideInSearch: true,
            render: (_, entity) => {
              return entity.timestamp;
            },
          },
          {
            title: "开始时间",
            dataIndex: "start_date",
            hideInTable: true,
            valueType: "dateTime",
            order: 3,
          },
          {
            title: "结束时间",
            dataIndex: "end_date",
            hideInTable: true,
            valueType: "dateTime",
            order: 2,
          },
          {
            title: "发送者",
            dataIndex: "sender",
            width: 200,
            ellipsis: true,
          },
          {
            title: "接收者",
            dataIndex: "recipient",
            width: 200,
            ellipsis: true,
          },

          {
            search: false,
            title: "操作",
            align: "center",
            dataIndex: "action",
            render: (_, entity) => {
              return (
                <Button
                  type="link"
                  onClick={() => {
                    deatilData.record = entity;
                    deatilData.open = true;
                  }}
                >
                  查看详细
                </Button>
              );
            },
          },
        ]}
        request={async (params, sort, filter) => {
          const res: any = await getReceiveListHttp({
            ...params,
            size: params.pageSize,
          });
          if (!res.status) {
            message.destroy();
            message.error(res.msg);
          }
          return {
            data: res.data.emails,
            total: res.data.total,
            success: res.status,
          };
        }}
        rowKey="id"
      ></ProTable>
    </Card>
  );
}
