import { Modal } from "antd";
import resso from "resso";
export const deatilData = resso<{ open: boolean; record: any }>({
  open: false,
  record: {},
});
export default function DeatilModal() {
  const { open, record } = deatilData;
  return (
    <Modal
      open={open}
      onCancel={() => (deatilData.open = false)}
      footer={null}
      width={"80vw"}
      style={{ minWidth: "800px" }}
    >
      <div style={{ height: "70vh", minHeight: "200px", overflow: "auto" }}>
        <div>收件人：{record.recipient}</div>
        <div>发件人：{record.sender}</div>
        <div>主题：{record.subject}</div>
        <div className="mt-1m pt-1m" style={{  borderTop: "1px solid #ccc" }}>
          <div dangerouslySetInnerHTML={{ __html: record.content }}></div>
        </div>
      </div>
    </Modal>
  );
}
