import EditorPro from "@/components/EditorPro";
import { sendEmailHttp } from "@/http";
import { Button, Card, Input, message } from "antd";
import { useState } from "react";
import DeatilModal from "../Send/DeatilModal";

export default function WriteEmail() {
  const [senderEmail, setSenderEmail] = useState("");
  const [receiverEmail, setReceiverEmail] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const sendEmail = () => {
    message.destroy();
    if (!senderEmail.trim()) {
      message.error("请输入发件人邮箱");
      return;
    }
    if (!receiverEmail.trim()) {
      message.error("请输入收件人邮箱");
      return;
    }
    if (!title.trim()) {
      message.error("请输入主题");
      return;
    }
    if (!content.trim()) {
      message.error("请输入内容");
      return;
    }
    sendEmailHttp({
      from: senderEmail,
      to: receiverEmail,
      subject: title,
      content,
    }).then((res: any) => {
      console.log(res);
      if (res?.status) {
        message.success("发送成功");
        setSenderEmail("");
        setReceiverEmail("");
        setTitle("");
        setContent("");
      } else {
        message.error(res?.msg || "发送失败");
      }
    });
  };
  return (
    <Card>
      <div className="flex flex-col mb-1">
        <div className="flex mb-1m" style={{ gap: 10 }}>
          <Input
            placeholder="请输入发件人邮箱"
            prefix={"发件人邮箱："}
            value={senderEmail}
            onChange={(e) => setSenderEmail(e.target.value)}
          />
          <Input
            placeholder="请输入收件人邮箱"
            prefix={"收件人邮箱："}
            value={receiverEmail}
            onChange={(e) => setReceiverEmail(e.target.value)}
          />
        </div>
        <Input
          placeholder="请输入主题"
          prefix={"主题："}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div style={{ minHeight: "550px" }}>
        <EditorPro
          className="h-3x"
          value={content}
          onChange={(e) => setContent(e)}
        />
      </div>
      <div className="flex flex-x-end">
        <Button type="primary" size="large" onClick={sendEmail}>
          发送
        </Button>
      </div>
    </Card>
  );
}
