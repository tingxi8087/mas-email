import {
  generateTokenHttp,
  getEmailServerStatusHttp,
  getKeyHttp,
  getUserInfoHttp,
  isRegHttp,
  loginHttp,
  regHttp,
  startEmailServerHttp,
  stopEmailServerHttp,
} from "@/http";
import { setSideNavHide, setTopNavHide } from "@/utils/setLayout";
import { Card, Form, Input, Table, Typography } from "antd";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, message, Steps, theme } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
const { Paragraph } = Typography;
const MainStep: React.FC<{
  isReg: boolean;
  setIsReg: Dispatch<SetStateAction<boolean>>;
}> = ({ isReg, setIsReg }) => {
  const { token } = theme.useToken();
  const [current, setCurrent] = useState(0);
  const [oneLoading, setOneLoading] = useState(false);
  const [towLoading, setTowLoading] = useState(false);
  const [emailServerStatus, setEmailServerStatus] = useState("停止");
  const [fourLoading, setFourLoading] = useState(false);
  const navigate = useNavigate();
  const [keyObj, setKeyObj] = useState<{
    privateKey: string;
    publicKey: string;
  }>({
    privateKey: "",
    publicKey: "",
  });
  const [domain, setDomain] = useState("");
  const [ip, setIp] = useState("");
  useEffect(() => {
    if (isReg) {
      setCurrent(1);
    }
  }, [isReg]);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };
  const getEmailServerStatus = () => {
    setFourLoading(true);
    getEmailServerStatusHttp().then((res) => {
      message.destroy();
      message.success("刷新成功");
      setFourLoading(false);
      setEmailServerStatus(res.status ? "运行中" : "停止");
    });
  };
  const steps = [
    {
      title: "注册账户",
      disabled: isReg,
      content: (
        <div>
          <div className="flex flex-x-c">
            <Form
              name="login"
              onFinish={(values) => {
                if (values.password !== values.password2) {
                  message.error("两次密码不一致");
                  return;
                }
                setOneLoading(true);
                regHttp(values).then((res) => {
                  setOneLoading(false);
                  if (res.status) {
                    message.success("注册成功");
                    loginHttp(values).then((res) => {
                      if (res.status) {
                        localStorage.setItem("token", res.data?.token);
                        setIsReg(true);
                      } else {
                        message.error("登录失败");
                      }
                    });
                  } else {
                    message.error("注册失败");
                  }
                });
              }}
              autoComplete="off"
              className="w-2x m-1 mt-4"
            >
              <Form.Item
                name="username"
                rules={[{ required: true, message: "请输入用户名!" }]}
              >
                <Input
                  prefix={<UserOutlined />}
                  placeholder="用户名"
                  size="large"
                  disabled={oneLoading}
                />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[{ required: true, message: "请输入密码!" }]}
              >
                <Input.Password
                  prefix={<LockOutlined />}
                  placeholder="密码"
                  size="large"
                  disabled={oneLoading}
                />
              </Form.Item>
              <Form.Item
                name="password2"
                rules={[{ required: true, message: "请输入密码!" }]}
              >
                <Input.Password
                  prefix={<LockOutlined />}
                  placeholder="确认密码"
                  size="large"
                  disabled={oneLoading}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  block
                  size="large"
                  loading={oneLoading}
                >
                  注册
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      ),
    },
    {
      title: "生成密钥",
      content: (
        <div>
          <Button
            size="large"
            type="primary"
            loading={towLoading}
            onClick={() => {
              setTowLoading(true);
              generateTokenHttp().then((res) => {
                if (res.status) {
                  message.success("生成成功");
                  setTowLoading(false);
                  setCurrent(2);
                  setKeyObj(res.data);
                }
              });
            }}
          >
            点击生成密钥
          </Button>
        </div>
      ),
    },
    {
      title: "配置服务器",
      content: (
        <div>
          <Card className="flex" size="small">
            <div className="flex w-100-pct">
              <Input
                placeholder="请输入"
                prefix="域名地址"
                className="mr-1 w-3x"
                value={domain}
                onChange={(e) => setDomain(e.target.value)}
              />
              <Input
                placeholder="请输入"
                prefix="主机IP"
                className="mr-1 w-3x"
                value={ip}
                onChange={(e) => setIp(e.target.value)}
              />
              <Button
                onClick={() => {
                  getKeyHttp().then((res: any) => {
                    if (res.status) {
                      setKeyObj(res.data);
                      message.destroy();
                      message.success("获取成功");
                    } else {
                      message.destroy();
                      message.error("请先生成公钥");
                    }
                  });
                }}
              >
                获取公钥
              </Button>
            </div>
          </Card>
          <div className="flex p-1m">
            <Card className="flex-1 mr-1m" size="small" title="DNS配置">
              <div className="text-left w-100-pct">
                <Table
                  size="small"
                  style={{ width: "100%" }}
                  rowKey="value"
                  columns={[
                    {
                      title: "主机记录",
                      dataIndex: "host",
                    },
                    {
                      title: "记录类型",
                      dataIndex: "type",
                    },
                    {
                      title: "记录值",
                      dataIndex: "value",
                      width: "60%",
                      render: (value, record) => {
                        return (
                          <div className="flex">
                            <div
                              style={{
                                textWrap: "wrap",
                                overflow: "hidden",
                                whiteSpace: "wrap",
                                wordBreak: "break-all",
                                // textOverflow: "ellipsis",
                                maxWidth: "400px",
                              }}
                            >
                              {value}
                            </div>
                            <Paragraph copyable={{ text: value }} />
                          </div>
                        );
                      },
                    },
                    {
                      title: "其他参数",
                      dataIndex: "extra",
                    },
                  ]}
                  dataSource={[
                    {
                      host: "@",
                      type: "MX",
                      value: `mail.${domain}`,
                      extra: "优先级：10",
                    },
                    {
                      host: "@",
                      type: "TXT",
                      value: `v=spf1 ip4:${ip} -all`,
                    },
                    {
                      host: "_dmarc",
                      type: "TXT",
                      value: `v=DMARC1; p=none; rua=mailto:postmaster@${domain}`,
                    },
                    {
                      host: "default._domainkey",
                      type: "TXT",
                      value: `v=DKIM1; k=rsa; p=${keyObj.publicKey
                        .replace(/\n/g, "")
                        .trim()
                        .replace("-----BEGIN PUBLIC KEY-----", "")
                        .replace("-----END PUBLIC KEY-----", "")}`,
                    },
                  ]}
                ></Table>
              </div>
            </Card>
            {/* <div className="w-2x">
              <Card title="公钥" size="small">
                <div
                  style={{
                    textAlign: "left",
                    boxOrient: "vertical",
                    textOverflow: "ellipsis",
                    display: "-webkit-box",
                    wordBreak: "break-all",
                  }}
                >
                  <Paragraph copyable>{keyObj.publicKey}</Paragraph>
                </div>
              </Card>
            </div> */}
          </div>
        </div>
      ),
    },
    {
      title: "启动服务",
      content: (
        <Card className="text-left">
          <div className="flex flex-x-sb flex-y-c">
            <div>
              邮箱服务状态：
              <span
                style={{
                  color: emailServerStatus == "运行中" ? "green" : "red",
                }}
              >
                {emailServerStatus}
              </span>
            </div>
            <Button
              onClick={() => getEmailServerStatus()}
              loading={fourLoading}
            >
              刷新状态
            </Button>
          </div>
          <div className="flex">
            <Button
              type="primary"
              onClick={() => {
                setFourLoading(true);
                startEmailServerHttp().then((res) => {
                  setFourLoading(false);
                  if (res.status) {
                    message.destroy();
                    message.success("启动成功");
                    getEmailServerStatus();
                  }
                });
              }}
              loading={fourLoading}
            >
              启动服务
            </Button>
            <Button
              className="ml-1"
              onClick={() => {
                setFourLoading(true);
                stopEmailServerHttp().then((res) => {
                  setFourLoading(false);
                  if (res.status) {
                    message.destroy();
                    message.success("停止成功");
                    getEmailServerStatus();
                  }
                });
              }}
              loading={fourLoading}
            >
              停止服务
            </Button>
          </div>
        </Card>
      ),
    },
  ];
  const items = steps.map((item) => ({ key: item.title, title: item.title }));

  const contentStyle: React.CSSProperties = {
    lineHeight: "260px",
    textAlign: "center",
    color: token.colorTextTertiary,
    backgroundColor: token.colorFillAlter,
    borderRadius: token.borderRadiusLG,
    border: `1px dashed ${token.colorBorder}`,
    marginTop: 16,
  };

  return (
    <>
      <Steps current={current} items={items} />
      <div style={contentStyle}>{steps[current].content}</div>
      <div style={{ marginTop: 24 }} className="flex flex-x-end">
        <Button
          style={{ margin: "0 8px" }}
          onClick={() => prev()}
          disabled={current == 0 || (isReg && current == 1)}
        >
          上一步
        </Button>
        {current < steps.length - 1 && (
          <Button
            type="primary"
            onClick={() => next()}
            disabled={!isReg && current == 0}
          >
            下一步
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button
            type="primary"
            onClick={() => {
              navigate("/index");
            }}
          >
            完成
          </Button>
        )}
      </div>
    </>
  );
};

export default function SetConfig() {
  const navigate = useNavigate();
  const [isReg, setIsReg] = useState(false);
  const [isShow, setIsShow] = useState(false);
  setSideNavHide();
  setTopNavHide();
  useEffect(() => {
    isRegHttp().then((res) => {
      if (res.status) {
        getUserInfoHttp().then((res) => {
          if (!res.status) {
            message.error("请先登录");
            setTimeout(() => {
              navigate("/login");
            }, 3000);
          } else {
            setIsShow(true);
            setIsReg(!!res.status);
          }
        });
      } else {
        setIsReg(!!res.status);
        setIsShow(true);
      }
    });
  }, []);

  return (
    <Card
      title={
        <div className="flex flex-x-sb flex-y-c">
          <div>欢迎使用mas-email</div>
          <div>
            {isReg && (
              <Button onClick={() => navigate("/index")}>返回首页</Button>
            )}
          </div>
        </div>
      }
      className="mr-1"
    >
      {isShow ? <MainStep isReg={isReg} setIsReg={setIsReg} /> : <div></div>}
    </Card>
  );
}
