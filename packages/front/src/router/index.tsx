import { AppstoreOutlined } from "@ant-design/icons";
import { Navigate, createHashRouter } from "react-router-dom";
import { wrapRoutesWithAuth } from "@/.utils/access";
import { getReactRouter, useLayout } from "@/.utils/routerRender";
import Index from "@/views/index";
import Login from "@/views/Login/Login";
import Page403 from "@/views/403";
import Page404 from "@/views/404";
import SetConfig from "@/views/SetConfig/SetConfig";
import Receive from "@/views/Receive/Receive";
import Send from "@/views/Send/Send";
import Settings from "@/views/Settings/Settings";
import WriteEmail from "@/views/WriteEmail/WriteEmail";

// import { HashRouter } from "react-router-dom";
const Router: MasRouter = [
  {
    hideMenu: true,
    path: "/",
    element: <Navigate to={"/index"} />,
  },

  {
    label: "首页",
    path: "/index",
    element: <Index />,
    access: "admin",
  },

  {
    label: "收件箱",
    path: "/receive",
    element: <Receive />,
    access: "admin",
  },
  {
    label: "写信",
    path: "/writeEmail",
    element: <WriteEmail />,
    access: "admin",
  },
  {
    label: "发件箱",
    path: "/send",
    element: <Send />,
    access: "admin",
  },
  {
    label: "个人设置",
    path: "/settings",
    element: <Settings />,
    access: "admin",
  },
  {
    path: "/login",
    element: <Login />,
    hideMenu: true,
  },

  {
    label: "设置页面",
    hideMenu: true,
    path: "/setConfig",
    element: <SetConfig />,
  },
  {
    path: "/403",
    element: <Page403 />,
    hideMenu: true,
  },
  {
    path: "*",
    element: <Page404 />,
    hideMenu: true,
  },
];

const { reactRouter, accessArr } = getReactRouter(
  useLayout(wrapRoutesWithAuth(Router))
);
// eslint-disable-next-line react-refresh/only-export-components
export const router = createHashRouter(reactRouter);
// eslint-disable-next-line react-refresh/only-export-components
export const routerAccessData = accessArr;
export const RouterIndex = Router;
