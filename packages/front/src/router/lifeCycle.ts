import { getUserInfoHttp } from "@/http";
import { layoutConfig } from "@/layout/layoutConfig";
import { accessStore } from "@/store/sys";
import { NavigateFunction, Location } from "react-router-dom";
function sleep(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
export async function befroeCreated() {
  accessStore.list = ["admin"];
  return true;
}
export const beforePageChange = async (
  navigate: NavigateFunction,
  location: Location<any>
) => {
  if (location.pathname === "/login" || location.pathname === "/setConfig")
    return true;
  const res = await getUserInfoHttp();
  if (!res?.data?.username && location.pathname !== "/login") {
    navigate("/login");
    return false;
  } else {
    layoutConfig.userName = res?.data?.username;
  }
  const { aceessValid } = await import("@/.utils/access");
  aceessValid(location, navigate);
  return true;
};
