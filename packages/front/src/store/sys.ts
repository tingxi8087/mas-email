import resso from "resso";

export const layoutStore = resso({
  sideNavHide: false,
  topNavHide: false,
});
export const accessStore = resso<any>({ list: [] });
