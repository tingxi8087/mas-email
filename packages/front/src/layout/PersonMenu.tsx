import React from "react";
import { UserOutlined } from "@ant-design/icons";
import { Avatar, MenuProps } from "antd";
import { Dropdown, Space } from "antd";
import { useNavigate } from "react-router-dom";
import { layoutConfig } from "./layoutConfig";
const PersonMenu: React.FC = () => {
  const { userName } = layoutConfig;
  const navigate = useNavigate();
  const items: MenuProps["items"] = [
    {
      key: "1",
      label: "退出登录",
      onClick: () => {
        localStorage.removeItem("token");
        navigate("/login", { replace: true });
      },
    },
  ];
  return (
    <div className="mr-1 pointer h-1mx flex flex-y-c">
      <Dropdown menu={{ items }} placement="bottom">
        <Space>
          <Avatar
            style={{ backgroundColor: "#1677ff" }}
            icon={<UserOutlined />}
          />
          {userName || "未命名用户"}
        </Space>
      </Dropdown>
    </div>
  );
};

export default PersonMenu;
