FROM nikolaik/python-nodejs:python3.12-nodejs23-slim
RUN npm install -g cnpm --registry=https://registry.npmmirror.com

# 验证安装的版本
RUN node --version && python --version

# 设置工作目录
WORKDIR /app

# 复制项目文件
COPY ./packages/backend /app
RUN mkdir -p /app/data
# 设置时区
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# 安装依赖
RUN cnpm install

# 如果有 Python 依赖，可以使用清华源加速
RUN pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN pip install aiosmtpd Flask dnspython dkimpy gunicorn
# 如果有 requirements.txt，取消注释下面这行
# RUN pip install -r requirements.txt

# 设置默认命令
CMD ["npm","run", "serve"]